<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class VentaProducto extends Model
{
    protected $table = 'venta_productos';


    public function producto()
	{
		return $this->belongsTo(Producto::class, 'producto_id');
	}

}

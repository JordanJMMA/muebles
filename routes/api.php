<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group(['namespace' => 'App'], function()
{
    Route::get('/x', 'AccesoController@create');
    Route::get('/pagos-cronjob', 'CobradorController@registro_pagos');
    Route::post('/login', 'AccesoController@login');

    Route::get('/get-cobradores', 'CobradorController@get_cobradores');
    Route::get('/get-data-cobrador', 'CobradorController@get_data_cobrador');
    Route::post('/sincronizar', 'CobradorController@sincronizar');
});


Route::group(['namespace' => 'App', 'middleware' => 'auth:sanctum'], function () {

});
